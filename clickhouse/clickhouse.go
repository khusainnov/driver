package clickhouse

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
)

var (
	maxRetries    = 3
	retryDuration = time.Second * 5
)

type ConfigCH struct {
	Addr []string
	Auth struct {
		Database string
		Username string
		Password string
	}
	DialTimeout     time.Duration // default 30 second
	MaxOpenConns    int           // default MaxIdleConns + 5
	MaxIdleConns    int           // default 5
	ConnMaxLifetime time.Duration // default 1 hour
}

// NewClickHouseDB creates connection with CH database
func NewClickHouseDB(cfg ConfigCH) (driver.Conn, error) {
	conn, err := clickhouse.Open(&clickhouse.Options{
		Addr: cfg.Addr,
		Auth: clickhouse.Auth{
			Database: cfg.Auth.Database,
			Username: cfg.Auth.Username,
			Password: cfg.Auth.Password,
		},
		DialTimeout:     cfg.DialTimeout,
		MaxOpenConns:    cfg.MaxOpenConns,
		MaxIdleConns:    cfg.MaxIdleConns,
		ConnMaxLifetime: cfg.ConnMaxLifetime,
	})
	for err != nil {
		maxRetries--

		conn, err = clickhouse.Open(&clickhouse.Options{
			Addr: cfg.Addr,
			Auth: clickhouse.Auth{
				Database: cfg.Auth.Database,
				Username: cfg.Auth.Username,
				Password: cfg.Auth.Password,
			},
			DialTimeout:     cfg.DialTimeout,
			MaxOpenConns:    cfg.MaxOpenConns,
			MaxIdleConns:    cfg.MaxIdleConns,
			ConnMaxLifetime: cfg.ConnMaxLifetime,
		})

		if maxRetries <= 0 {
			return nil, err
		}

		time.Sleep(retryDuration)
	}

	if err = conn.Ping(context.Background()); err != nil {
		return nil, err
	}

	return conn, nil
}
