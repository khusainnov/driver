package redis

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

var (
	maxRetries    = 3
	retryDuration = time.Second * 5
)

// ConfigRedis implements all values for get redis client
type ConfigRedis struct {
	Port     string
	Password string
	DB       int
}

func NewRedisDB(cfg ConfigRedis, ctx context.Context) (*redis.Client, error) {
	dbr := redis.NewClient(&redis.Options{
		Addr:     cfg.Port,
		Password: cfg.Password,
		DB:       cfg.DB,
	})

	_, err := dbr.Ping(ctx).Result()
	for err != nil {
		maxRetries--

		dbr = redis.NewClient(&redis.Options{
			Addr:     cfg.Port,
			Password: cfg.Password,
			DB:       cfg.DB,
		})

		_, err = dbr.Ping(ctx).Result()

		if maxRetries <= 0 {
			return nil, err
		}

		time.Sleep(retryDuration)
	}

	return dbr, err
}
