package postgres

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var (
	maxRetries    = 3
	retryDuration = time.Second * 5
)

// ConfigPG config struct
type ConfigPG struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
	SSLMode  string

	// setup
	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxIdleTime time.Duration
	ConnMaxLifetime time.Duration
}

type Client struct {
	db *sqlx.DB
}

func NewPostgresDB(cfg ConfigPG) (*Client, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=%v",
		cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.DBName, cfg.SSLMode))
	for err != nil {
		maxRetries--
		db, err = sqlx.Open("postgres", fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=%v",
			cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.DBName, cfg.SSLMode))

		if maxRetries == 0 {
			return nil, err
		}

		time.Sleep(retryDuration)
	}

	// Ping verifies a connection to the database is still alive, establishing a connection if necessary.
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	// SetMaxOpenConns sets the maximum number of open connections to the database
	// The default is 0 (unlimited).
	db.SetMaxOpenConns(cfg.MaxOpenConns)

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	// The default max idle connections is currently 2.
	db.SetMaxIdleConns(cfg.MaxIdleConns)

	// SetConnMaxIdleTime sets the maximum amount of time a connection may be idle.
	// If d <= 0, connections are not closed due to a connection's idle time.
	db.SetConnMaxIdleTime(cfg.ConnMaxIdleTime)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	// If d <= 0, connections are not closed due to a connection's age.
	db.SetConnMaxLifetime(cfg.ConnMaxLifetime)

	return &Client{db: db}, nil
}
