package postgres

import (
	"context"
	"database/sql"
	"fmt"
)

type Query interface {
	Query(ctx context.Context, query, methodName string, args ...interface{}) (*sql.Rows, error)
}

func (c *Client) Query(ctx context.Context, query, methodName string, args ...interface{}) (*sql.Rows, error) {
	rows, err := c.db.QueryContext(ctx, query, args)
	if err != nil {
		return nil, fmt.Errorf("failed to query: %v, err: %w", methodName, err)
	}

	return rows, nil
}
